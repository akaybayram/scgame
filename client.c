#include "game.h"
#include "config.h"
#define SAL_QUEUE_IMPLEMENTATION
#include "./sal/sal.h"

// TODO: solve race condition in sender and listener

Player self = {0};
Game game = {0};
char address[13];
char port[7];
int sd;
struct addrinfo hints;
struct addrinfo *res;

SalQueue *recv_queue;
SalQueue *send_queue;

pthread_mutex_t recv_queue_mutex;

char *read_config_file(){
    FILE *config = fopen("config", "r");

    fseek(config, 0, SEEK_END);
    long size = ftell(config);
    fseek(config, 0, SEEK_SET);

    char *mem = malloc(size);
    if(fread(mem, 1, size, config) != (size_t)size){
        printf("Dosya okuma başarısız!\n");
        fclose(config);
        free(mem);
        return NULL;
    }

    fclose(config);
    return mem;
}

void draw_players(Game *game){
    for(int i = 0; i < game->player_count; ++i){
        DrawRectangle(game->players[i].pos.x,
                      game->players[i].pos.y,
                      20, 20,
                      game->players[i].color);
        DrawText(game->players[i].name,
                 game->players[i].pos.x - 5, // 5 = size / 2
                 game->players[i].pos.y + 5, // 5 = size / 2
                 10, YELLOW);
    }
}

void *sender_thread(void *args){
    (void)args;

    while(true){
        if(sal_queue_size(send_queue) > 0){
            Event ev;
            sal_queue_take(send_queue, &ev);
            sendto(sd, &ev, sizeof(Event), 0,
                   res->ai_addr, res->ai_addrlen);
        }
        usleep(1000);
    }
    return NULL;
}

void *connection_listener_thread(void *args){
    (void)args;

    struct sockaddr baglanti_adresi;
    socklen_t adres_uzunlugu;
    Event ev;

    while(1){
        ssize_t okunan = recvfrom(sd,
                                  &ev,
                                  sizeof(Event),
                                  0,
                                  &baglanti_adresi,
                                  &adres_uzunlugu);
        //assert(okunan == sizeof(Event));
        pthread_mutex_lock(&recv_queue_mutex);
        sal_queue_add(recv_queue, &ev);
        pthread_mutex_unlock(&recv_queue_mutex);
        usleep(1000);
    }

    return NULL;
}

void *event_handler(void *args){
    (void)args;

    while(1){
        Event ev;
        ev.type = EMPTY_EVENT;
        // Take event
        pthread_mutex_lock(&recv_queue_mutex);
        if(sal_queue_size(recv_queue) > 0){
            sal_queue_take(recv_queue, &ev);
        }
        pthread_mutex_unlock(&recv_queue_mutex);

        // Process event
        switch(ev.type){
        case CONNECTION_REPLY:
        {
            self.id = ev.id;
            printf("New id got %d.\n", self.id);
        }break;
        case UPDATE:
        {
            game.players[ev.player_info.id].pos = ev.player_info.pos;
            game.players[ev.player_info.id].color = ev.player_info.color;
            game.players[ev.player_info.id].speed = ev.player_info.speed;
        }break;
        case PLAYER_COUNT_UPDATE:
        {
            game.player_count = ev.player_count;
        }break;
        case PLAYER_NAME_UPDATE:
        {
            strcpy(game.players[ev.name_info.id].name, ev.name_info.name);
        }break;
        case UPDATE_GAME:
        {
            memcpy(&game, &ev.game_info, sizeof(Game));
        }break;
        case EMPTY_EVENT: // No event
            break;
        default:
            printf("Invalid event from: %d\nEvent : %d\n", ev.id, ev.type);
            break;
        }
        usleep(1000);
    }

    return NULL;
}

int main(){
    send_queue = sal_queue_new(sizeof(Event));
    recv_queue = sal_queue_new(sizeof(Event));

    pthread_mutex_init(&recv_queue_mutex, NULL);
    // Read config
    {
        char *config, *line, *key, *line_saveptr, *key_saveptr;
        config = read_config_file();

        line = strtok_r(config, "\n", &line_saveptr);
        while(line != NULL){
            key = strtok_r(line, ":", &key_saveptr);
                if(!strcmp(key, "Color")){
                    char *mycolor = strtok_r(NULL, ":", &key_saveptr);
                    for(int i = 0; i < enum_COLOR_COUNT; ++i){
                        if(strcmp(mycolor, color_string[i]) == 0){
                            self.color = color[i];
                        }
                    }
                }else if(!strcmp(key, "Name")){
                    char *myname = strtok_r(NULL, ":", &key_saveptr);
                    strcpy(self.name, myname);
                }
                else if(!strcmp(key, "Ip")){
                    char *myaddress = strtok_r(NULL, ":", &key_saveptr);
                    strcpy(address, myaddress);
                }
                else if(!strcmp(key, "Port")){
                    char *myport = strtok_r(NULL, ":", &key_saveptr);
                    strcpy(port, myport);
                }
            line = strtok_r(NULL, "\n", &line_saveptr);
        }

        free(config);
    }
    // network conn
    hints.ai_flags = AI_NUMERICSERV;
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = 0;
    int sonuc = getaddrinfo(address, port, &hints, &res);
    if (sonuc != 0){
        printf("Getaddrinfo error!\n%d\n", sonuc);
        return -1;
    }

    sd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sd < 0){
        fprintf(stderr, "Socket error!\n");
        return -1;
    }
    //
    Event connect_ev = {0};
    connect_ev.type = CONNECT;
    strcpy(connect_ev.conn_info.name, self.name);
    connect_ev.conn_info.color = self.color;

    sal_queue_add(send_queue, &connect_ev);

    SetTargetFPS(60);
    InitWindow(500, 500, "Client");
    pthread_t th[3];
    pthread_create(&th[0], NULL, sender_thread, NULL);
    pthread_create(&th[1], NULL, connection_listener_thread, NULL);
    pthread_create(&th[2], NULL, event_handler, NULL);

    Event control_ev;

    while(!WindowShouldClose()){
        control_ev.id = self.id;
        control_ev.type = MOVE;
        control_ev.move.mov_x = (char)(IsKeyDown(KEY_D) - IsKeyDown(KEY_A));
        control_ev.move.mov_y = (char)(IsKeyDown(KEY_S) - IsKeyDown(KEY_W));
        if(!(control_ev.move.mov_x == 0 && control_ev.move.mov_y == 0)){
            sal_queue_add(send_queue, &control_ev);
        }

        BeginDrawing();
        ClearBackground((Color){61,61,61,255});
        draw_players(&game);
        EndDrawing();
    }

    return 0;
}
