CC=gcc
CFLAGS=-Wall -Wextra -g
LFLAGS=-lraylib -lpthread
FILES=$(wildcard *.c)
OBJ=$(patsubst %.c,%.o,$(FILES))
TARGET= server client


all: server

$(OBJ): %.o: %.c
	$(CC) -c -o $@ $< $(CFLAGS) $(LFLAGS)

$(TARGET): $(OBJ)
	$(CC) $@.o -o $@ $(CFLAGS) $(LFLAGS)

clean:
	rm *.o
	rm $(TARGET)
