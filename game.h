#ifndef GAME_H_
#define GAME_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/time.h>
#include <errno.h>
#include <pthread.h>
#include <raylib.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/ip.h> /* superset of previous */
#include <sys/uio.h>

#define MAX_PLAYER_COUNT 10
#define MAX_PLAYER_NAME_SIZE 64

typedef struct{
    int id;
    char name[MAX_PLAYER_NAME_SIZE];
    Color color;
    Vector2 pos;
    uint speed;
    struct sockaddr adres;
}Player;

typedef struct{
    Player players[MAX_PLAYER_COUNT];
    short player_count;
}Game;

typedef enum{
    CONNECT = 0b10101010,
    CONNECTION_REPLY,
    MOVE,
    UPDATE,
    UPDATE_GAME,
    PLAYER_COUNT_UPDATE,
    PLAYER_NAME_UPDATE,
    EMPTY_EVENT,
} EventType;

typedef struct{
    int id;
    char name[MAX_PLAYER_NAME_SIZE];
}PlayerNameInfo;

typedef struct{
    int id;
    Vector2 pos;
    Color color;
    uint speed;
}PlayerInfo;

typedef struct{
    char mov_x;
    char mov_y;
}MoveInfo;

typedef struct{
    char name[MAX_PLAYER_NAME_SIZE];
    Color color;
} ConnectInfo;

typedef struct{
    bool status;
}ConnectReply;

typedef struct{
   EventType type;
   int id;
   struct sockaddr adres;
   union{
       MoveInfo move;
       ConnectInfo conn_info;
       ConnectReply conn_reply_info;
       PlayerInfo player_info;
       PlayerNameInfo name_info;
       short player_count;
       Game game_info;
   };
}Event;

#endif // GAME_H_
