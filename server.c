#include "game.h"
#define SAL_QUEUE_IMPLEMENTATION
#include "./sal/sal.h"

Vector2 default_spawn_pos = {.x = 0.0f, .y = 0.0f};
uint default_speed = 100;
Game game = {0};
pthread_mutex_t send_queue_mutex;
SalQueue *recv_queue;
SalQueue *send_queue;
int sd; // Connection socket
uint UPS = 60; // Update per second

void draw_players(Game *game){
    for(int i = 0; i < game->player_count; ++i){
        DrawRectangle(game->players[i].pos.x,
                      game->players[i].pos.y,
                      20, 20,
                      game->players[i].color);
    }
}

void move_player(Event *ev){
    game.players[ev->id].pos.x +=
        (float)ev->move.mov_x * (float)game.players[ev->id].speed * (float)(1.0f/(float)UPS);
    game.players[ev->id].pos.y +=
        (float)ev->move.mov_y * (float)game.players[ev->id].speed * (float)(1.0f/(float)UPS);
}

void connect_player(Event *ev){
    Player yeni;
    yeni.id = game.player_count;
    printf("gonderilen id : %d\n", game.player_count);
    yeni.color = ev->conn_info.color;
    yeni.pos = default_spawn_pos;
    yeni.speed = default_speed;
    strcpy(yeni.name, ev->conn_info.name);
    yeni.adres = ev->adres;


    // Construct reply event
    Event reply_ev;
    reply_ev.type = CONNECTION_REPLY;
    reply_ev.id = yeni.id;
    reply_ev.conn_reply_info.status = true;
    reply_ev.adres = yeni.adres;
    pthread_mutex_lock(&send_queue_mutex);
    sal_queue_add(send_queue, &reply_ev); // add queue
    pthread_mutex_unlock(&send_queue_mutex);

    game.players[yeni.id] = yeni;
    game.player_count++;
}

void *server_sender_thread(void *args){
    (void)args;

    while(true){
        pthread_mutex_lock(&send_queue_mutex);
        if(sal_queue_size(send_queue) > 0){
            Event ev;
            sal_queue_take(send_queue, &ev);
            sendto(sd, &ev, sizeof(Event), 0,
                      (struct sockaddr*)&ev.adres, sizeof(struct sockaddr));
        }
        pthread_mutex_unlock(&send_queue_mutex);
        usleep(1000);
    }

    return NULL;
}

void *server_event_listener(void *args){
    (void)args;
    sd = socket(AF_INET, SOCK_DGRAM, 0);
    if(sd < 0){
        fprintf(stderr, "Error socket create!\n");
        return NULL;
    }

    struct sockaddr baglanti_adresi;
    Event ev;
    socklen_t adres_uzunlugu = sizeof(struct sockaddr);
    struct sockaddr_in addr;
    addr.sin_port = htons(6161);
    addr.sin_family = AF_INET;

    if(bind(sd,
            (struct sockaddr *)&addr,
             adres_uzunlugu) == -1){
        fprintf(stderr, "Bind error!\n%s\n", strerror(errno));
        return NULL;
    }
    while(1){
        ssize_t okunan = recvfrom(sd,
                                  &ev,
                                  sizeof(Event),
                                  0,
                                  &baglanti_adresi,
                                  &adres_uzunlugu);
        //assert(okunan == sizeof(Event));
        ev.adres = baglanti_adresi;
        sal_queue_add(recv_queue, &ev);
        usleep(1000);
    }

    return NULL;
}

void *event_handler(void *args){
    (void)args;
    while(1){
        if(sal_queue_size(recv_queue) > 0){
            Event ev;
            sal_queue_take(recv_queue, &ev);
            switch(ev.type){
            case CONNECT:
            {
                if(game.player_count == MAX_PLAYER_COUNT){
                    // TODO: Add server full event.
                    break;
                }
                connect_player(&ev);
                // Send count update for sync clients
                for(int i = 0; i < game.player_count; ++i){
                    Event ev = {0};
                    ev.type = PLAYER_COUNT_UPDATE;
                    ev.player_count = game.player_count;
                    ev.adres = game.players[i].adres;
                    pthread_mutex_lock(&send_queue_mutex);
                    sal_queue_add(send_queue, &ev);
                    pthread_mutex_unlock(&send_queue_mutex);
                }
                printf("Player successfully added!\nOyuncu ismi:%s\n", ev.conn_info.name);
                // Send name update for sync clients
                for(int i = 0; i < game.player_count; ++i){
                    memset(&ev, 0, sizeof(Event));
                    ev.type = PLAYER_NAME_UPDATE;
                    ev.adres = game.players[i].adres;
                    for(int i = 0; i < game.player_count; ++i){
                        ev.name_info.id = game.players[i].id;
                        strcpy(ev.name_info.name, game.players[i].name);
                        pthread_mutex_lock(&send_queue_mutex);
                        sal_queue_add(send_queue, &ev);
                        pthread_mutex_unlock(&send_queue_mutex);
                    }
                }
            }break;

            case MOVE:
            {
                if(ev.move.mov_x > 1 || ev.move.mov_x < -1 || ev.move.mov_y > 1 || ev.move.mov_y < -1){
                    break; // discard package
                }
                move_player(&ev);
            }break;

            default:
                printf("Invalid event from : %d\nEvent type: %d\n", ev.id, ev.type);
                break;
            }
        }
        usleep(1000);
    }

    return NULL;
}

int main(){
    recv_queue = sal_queue_new(sizeof(Event));
    send_queue = sal_queue_new(sizeof(Event));

    pthread_mutex_init(&send_queue_mutex, NULL);

    pthread_t th[3];
    pthread_create(&th[0], NULL, server_event_listener, NULL);
    pthread_create(&th[1], NULL, event_handler, NULL);
    pthread_create(&th[2], NULL, server_sender_thread, NULL);

    /*
    SetTargetFPS(60);
    InitWindow(500, 500, "Deneme");
    */
    while(true){//!WindowShouldClose()){
        // send game state to all clients
        struct timeval before, after;

        Event ev;
        ev.type = UPDATE_GAME;
        for(int i = 0; i < game.player_count; ++i){
            ev.adres = game.players[i].adres;
            ev.game_info = game;
            pthread_mutex_lock(&send_queue_mutex);
            sal_queue_add(send_queue, &ev);
            pthread_mutex_unlock(&send_queue_mutex);
        }

        gettimeofday(&after, NULL);
        before = after;
        uint update_time = after.tv_usec - before.tv_usec;
        uint wait_time = ((1000000/UPS) - update_time);
        usleep(wait_time); // match update rate with UPS

        /*
        BeginDrawing();
        ClearBackground((Color){61, 61, 61, 255});
        draw_players(&game);
        EndDrawing();
        */
    }

    return 0;
}
